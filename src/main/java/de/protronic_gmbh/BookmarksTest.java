
package de.protronic_gmbh;

import java.io.IOException;
import java.util.List;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfOutline;
import com.itextpdf.kernel.pdf.PdfReader;

/**
 * Hello world!
 *
 */
public class BookmarksTest {
	public static final String SRC = "./test/Assembly Drawings.PDF";

	public static void main(String[] args) throws IOException {
		PdfDocument pdfDoc = new PdfDocument(new PdfReader(SRC));

		PdfOutline outlines = pdfDoc.getOutlines(false);
		List<PdfOutline> bookmarks = outlines.getAllChildren().get(0).getAllChildren();
		StringBuffer stringBuffer = new StringBuffer();
		for (PdfOutline bookmark : bookmarks) {
			showTitle(bookmark, stringBuffer);
		}
		pdfDoc.close();

		System.out.println(stringBuffer.toString());
	}

	public static void showTitle(PdfOutline outline, StringBuffer stringBuffer) {
		System.out.println("huhu!");
		System.out.println(outline.getTitle());
		stringBuffer.append(outline.getTitle() + "\n");
		List<PdfOutline> kids = outline.getAllChildren();
		if (kids != null) {
			for (PdfOutline kid : kids) {
				showTitle(kid, stringBuffer);
			}
		}
	}
}
